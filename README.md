# Parallel Status Lines

[![crates.io](https://img.shields.io/crates/v/parsli.svg)](https://crates.io/crates/parsli)
[![rustc](https://img.shields.io/badge/rustc-nightly-lightgrey)](https://doc.rust-lang.org/nightly/std/)

**parsli** is an opinionated Rust library to visualize the execution of dependent tasks. It is designed to be as flexible as possible, while abstracting away the management of dependencies and command-line visualization.

## Quickstart
Everything is managed through the `ThreadPool` struct, which takes care of dependencies and status lines.

```rust
use parsli::ThreadPool;

// Execute 4 tasks in parallel
let mut pool = ThreadPool::new(4);
```

A task has a name, dependencies and some function it has to execute.
This function takes three arguments
 - `line`: The status line of this thread that can be controlled.
 - `name`: The name of this task.
 - `ctx`: A hashmap with results of the tasks this one depends oni.
This is a basic example on how to instantiate a task.

```rust
use std::thread::sleep;
use std::time::Duration;
use parsli::{Task, Line, Ctx};

let mytask = Task::new(|line: Line, name: String, ctx: Ctx<String>| {
    line.update_message("preparing...".to_string());
    sleep(Duration::from_millis(1000));
    line.update_message("processing...".to_string());
    sleep(Duration::from_millis(2000));
    Ok("this is the result".to_string())
});
```

Tasks can be added to the pool with the `ThreadPool::add_task` function.

```rust
pool.add_task("mytask", vec![], mytask);
```

When all tasks have been added to the pool, it can be started with `ThreadPool::start`.

```rust
pool.start();
```

## Full example

```rust
use std::thread::sleep;
use std::time::Duration;
use parsli::{Ctx, Line, ThreadPool, Task};

/// This is a minimal example on how to use parsli
fn main() {
    let mut pool = ThreadPool::new(4);
    let dummy = Task::new(|line: Line, name: String, ctx: Ctx<String>| {
        line.update_message("preparing...".to_string());
        sleep(Duration::from_millis(1000 + 10 * (rand::random::<u8>() as u64)));
        if name != "clang.install".to_string() {
            Ok("this is the result".to_string())
        } else {
            Err("this task went terribly wrong".to_string())
        }
    });
    pool.add_task("llvm.fetch", vec![], dummy.clone());
    pool.add_task("clang.configure", vec!["llvm.fetch"], dummy.clone());
    pool.add_task("compiler-rt.configure", vec!["llvm.fetch"], dummy.clone());
    pool.add_task("clang.compile", vec!["clang.configure"], dummy.clone());
    pool.add_task("compiler-rt.compile", vec!["compiler-rt.configure"], dummy.clone());
    pool.add_task("clang.install", vec!["clang.compile"], dummy.clone());
    pool.add_task("compiler-rt.install", vec!["compiler-rt.compile"], dummy.clone());
    pool.start();
}
```

![animation](assets/example.gif)
