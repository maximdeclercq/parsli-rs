use std::collections::HashMap;

/// Context provided for the tasks to execute
pub type Ctx<S> = HashMap<String, S>;

/// Manager for providing contexts to tasks
#[derive(Clone)]
pub struct CtxMgr<S> {
    /// Global context
    global: Ctx<S>,
}

impl<S> CtxMgr<S> where S: Clone {
    pub(crate) fn new() -> Self {
       Self { global: Ctx::default() }
    }

    /// Insert or update value into the context
    pub(crate) fn insert(&mut self, name: String, s: S) {
        self.global.insert(name, s);
    }

    /// Get context for a subset of tasks
    pub(crate) fn filter(&self, names: Vec<String>) -> Ctx<S> {
        names.iter().filter_map(|name| Some((name.clone(), (*self.global.get(name)?).clone()))).collect()
    }
}