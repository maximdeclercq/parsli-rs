#![feature(fn_traits, unboxed_closures, trait_alias, type_alias_impl_trait)]

pub use ctx::Ctx;
pub use pool::ThreadPool;
pub use task::Task;
pub use line::Line;
pub(crate) use line::LineStatus;

pub mod pool;
pub mod dep;
pub mod task;
pub mod line;
pub mod ctx;