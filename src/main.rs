use std::thread::sleep;
use std::time::Duration;
use parsli::{Ctx, Line, ThreadPool, Task};

/// This is a minimal example on how to use parsli
fn main() {
    let mut pool = ThreadPool::new(4);
    let dummy = Task::new(|line: Line, name: String, ctx: Ctx<String>| {
        line.update_message("preparing...".to_string());
        sleep(Duration::from_millis(1000 + 10 * (rand::random::<u8>() as u64)));
        if name != "clang.install".to_string() {
            Ok("this is the result".to_string())
        } else {
            Err("this task went terribly wrong".to_string())
        }
    });
    pool.add_task("llvm.fetch", vec![], dummy.clone());
    pool.add_task("clang.configure", vec!["llvm.fetch"], dummy.clone());
    pool.add_task("compiler-rt.configure", vec!["llvm.fetch"], dummy.clone());
    pool.add_task("clang.compile", vec!["clang.configure"], dummy.clone());
    pool.add_task("compiler-rt.compile", vec!["compiler-rt.configure"], dummy.clone());
    pool.add_task("clang.install", vec!["clang.compile"], dummy.clone());
    pool.add_task("compiler-rt.install", vec!["compiler-rt.compile"], dummy.clone());
    pool.start();
}
