/// Current state of a task
#[derive(Copy, Clone, Debug, PartialEq)]
pub(crate) enum State {
    /// The task is waiting to be scheduled
    Waiting,
    /// The task is scheduled
    Running,
    /// The task succeeded
    Success,
    /// The task failed
    Failure,
}


impl State {
    pub(crate) fn color(&self) -> String {
        match self {
            State::Waiting => "yellow",
            State::Running => "orange",
            State::Success => "green",
            State::Failure => "red",
        }.to_string()
    }
}